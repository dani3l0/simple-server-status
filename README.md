# Simple Server status

Simple web app to check machine's resources usage. Useful when building Android ROMs. For anything else, completely useless (it's PHP).

<img src="https://gitlab.com/dani3l0/simple-server-status/raw/main/screenshot.png">
