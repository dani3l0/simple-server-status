function update() {
	let xhr = new XMLHttpRequest();
	xhr.open("GET", "api.php", true);
	xhr.onload = function() {
		if (this.status == 200) {
			let resp = JSON.parse(this.responseText);
			console.log(resp)
			set("title", resp.hostname + " status");
			set("title2", resp.hostname);
			set("cpu_model", resp.cpu);
			setp("cpu_p", resp.cpu_p);
			set("cpu_p_", Math.round(resp.cpu_p), "%");
			set("mem_used", Math.round(resp.ram_u / 10) / 100);
			set("mem_total", Math.round(resp.ram_t / 10) / 100);
			setp("ram_p", resp.ram_p);
			set("ram_p_", Math.round(resp.ram_p), "%");
			set("s_used", Math.round(resp.disk_u / 10) / 100);
			set("s_total", Math.round(resp.disk_t / 10) / 100);
			setp("s_p", resp.disk_p);
			set("s_p_", Math.round(resp.disk_p), "%");
			set("l0", resp.loadavg[0]);
			set("l1", resp.loadavg[1]);
			set("l2", resp.loadavg[2]);
			setp("la_p", Math.round(resp.l_p));
			set("la_p_", Math.round(resp.l_p), "%");
			set("cores", resp.cores);
		}
	}
	xhr.send();
}
function set(id, val, e) {
	if (!e) e = "";
	document.getElementById(id).innerText = val + e;
}
function setp(id, val) {
	document.getElementById(id).style.width = `${val}%`;
}
update()
setInterval(update, 1200);
