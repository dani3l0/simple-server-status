<?php

// CPU
$cont = file('/proc/stat');
$cpuloadtmp = explode(' ',$cont[0]);
$cpuload0[0] = $cpuloadtmp[2] + $cpuloadtmp[4];
$cpuload0[1] = $cpuloadtmp[2] + $cpuloadtmp[4]+ $cpuloadtmp[5];
sleep(1);
$cont = file('/proc/stat');
$cpuloadtmp = explode(' ',$cont[0]);
$cpuload1[0] = $cpuloadtmp[2] + $cpuloadtmp[4];
$cpuload1[1] = $cpuloadtmp[2] + $cpuloadtmp[4]+ $cpuloadtmp[5];
foreach(file('/proc/cpuinfo') as $ri) {
	if ("model name" == substr($ri, 0, strlen("model name"))) {
		$cpu = substr($ri, strpos($ri, ":") + 1);
	}
}
$cpu_p = ($cpuload1[0] - $cpuload0[0])*100/($cpuload1[1] - $cpuload0[1]);


// Memory
foreach(file('/proc/meminfo') as $ri) {
	$m[strtok($ri, ':')] = strtok('');
}
$ram_t = intval(preg_replace('/[^0-9]+/', '', $m['MemTotal']), 10) / 1000;
$ram_u = $ram_t - ($m['MemFree'] + $m['Buffers'] + $m['Cached']) / 1000;
$ram_p = 100 - round(($m['MemFree'] + $m['Buffers'] + $m['Cached']) / $m['MemTotal'] * 100);


// Storage
$disk_t = round(disk_total_space("/") / 1000) / 1000;
$disk_f = round(disk_free_space("/") / 1000) / 1000;
$disk_u = $disk_t - $disk_f;
$disk_p = 100 - round(100 * $disk_f / $disk_t);

$hostname = file_get_contents("/etc/hostname");

// CPUs and load
$load = sys_getloadavg();
$cpuinfo = file_get_contents('/proc/cpuinfo');
preg_match_all('/^processor/m', $cpuinfo, $matches);
$cpus = count($matches[0]);
$e = 100 * $load[0] / $cpus;
if ($e >= 100) $e = 100;

echo json_encode(array(
	'cpu_p' => $cpu_p,
	'cpu' => $cpu,
	'ram_u' => $ram_u,
	'ram_p' => $ram_p,
	'ram_t' => $ram_t,
	'disk_t' => $disk_t,
	'disk_f' => $disk_f,
	'disk_u' => $disk_u,
	'disk_p' => $disk_p,
	'loadavg' => $load,
	'cores' => $cpus,
	'hostname' => $hostname,
	'l_p' => $e
));

?>
